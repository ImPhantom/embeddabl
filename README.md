# Embeddabl

A pretty terrible discord bot that allows users to convert/transcode unembeddable media into a format that discord will easily embed.

### Use Cases: *(eventually)*
* Convert most weird image formats (tga, tiff, jfif) into png/jpeg
* Simply compress images if they are too large
* Encode large videos into smaller format mp4 files
* Probably more...

## Contributing:
**Uhhhhh, I don't know...**
