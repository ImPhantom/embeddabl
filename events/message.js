module.exports = (client, message) => {
    let prefix = client.botConfig.prefix;

    // If the guild has a custom prefix set
    if (message.guild && client.guildConfigs.has(message.guild.id)) {
        const _guildConfig = client.guildConfigs.get(message.guild.id);
        prefix = _guildConfig.prefix;
    }

    if (!message.content.startsWith(prefix) || message.author.bot) return;
    const args = message.content.slice(prefix.length).split(/ +/);
    const command = args.shift().toLowerCase();

    if (!client.commands.has(command)) return;
    const _command = client.commands.get(command);

    // If the command requires permissions and is being run in a guild, verify the user has the permissions.
    if (message.guild && _command.requiredPermissions && !message.member.hasPermission(_command.requiredPermissions))
        return message.reply("*invalid_permissions*");

    // If the command is guild only, prevent from being run in a guild
    if (_command.guildOnly && !message.guild)
        return;

    try {
        const _command = client.commands.get(command);
        _command.run(client, message, args);
    } catch (error) {
        console.warn(`Error while running command: ${error}`);
        message.reply("Something went wrong when executing that command!");
    }
}