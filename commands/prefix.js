module.exports = {
    name: 'prefix',
    description: 'This command allows you to set a new prefix for the guild.',
    guildOnly: true,
    requiredPermissions: ["ADMINISTRATOR"],
    run(client, message, args) {
        let guildConfig = client.guildConfigs.ensure(message.guild.id, { prefix: "%" });

        if (args.length < 1) {
            return message.channel.send(`**You must specify a new prefix argument!**`);
        }

        guildConfig.prefix = args[0];
        client.guildConfigs.set(message.guild.id, guildConfig);
        message.channel.send(`**My new prefix for this guild is **\`${args[0]}\``);
    }
}