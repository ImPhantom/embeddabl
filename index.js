const fs = require("fs");

// Really bootleg method to try and load the configuration.
let botConfig;
try {
    botConfig = require("./data/config.json");
} catch (error) {
    console.log("Couldn't find configuration file at 'data/config.json', bot cannot load!");
    console.log('(Process will exit in 5 seconds)');
    return setTimeout(() => process.exit(), 5000);
}

const { Client, Collection } = require("discord.js");
const client = new Client();

client.botConfig = botConfig;
client.commands = new Collection();

const Enmap = require("enmap");
client.guildConfigs = new Enmap({ name: "guildConfigs", fetchAll: false, autoFetch: true, cloneLevel: "deep" });

// Load commands
fs.readdir("./commands/", (error, files) => {
    if (error) return console.error(error);

    files.forEach(_file => {
        const _command = require(`./commands/${_file}`);
        client.commands.set(_command.name, _command);
    });
});

// Load event handlers
fs.readdir("./events/", (error, files) => {
    if (error) return console.error(error);

    files.forEach(_file => {
        const _event = require(`./events/${_file}`);
        client.on(_file.split(".")[0], _event.bind(null, client));
    });
});

client.login(botConfig.token);